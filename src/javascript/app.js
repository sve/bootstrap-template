var Template = (function() {
    var $background = $(".bg-backstretch"),
        $sections = $('.site-wrapper'),
        $header = $('.navbar'),
        $clients = $("#owl-clients"),
        $map = $("#map"),
        lat = $map.attr('data-lat'),
        lng = $map.attr('data-lng'),
        bgPath = 'images/backgrounds/';

    /* Fade content on page loads */
    $(window).load(function() {
        $('.site-cover').addClass('onload');
    });

    /* Section backgrounds */
    $background.backstretch([
        bgPath + "Background-image-1.jpg",
        bgPath + "Background-image-2.jpg",
        bgPath + "Background-image-3.jpg",
        bgPath + "Background-image-4.jpg",
        bgPath + "Background-image-5.jpg",
        bgPath + "Background-image-6.jpg",
        bgPath + "Background-image-7.jpg"
    ], {
        fade: 500
    }).backstretch("pause");

    /* Fade background on section changes */
    $sections.waypoint(function(direction) {
        if (direction === 'down') {
            $background.backstretch("next");
        } else if (direction === 'up') {
            $background.backstretch("prev");
        }
    }, {
        offset: '30%',
        enabled: false
    }).waypoint('enable');


    /* Sticky header when scroll > 100px
     * Always sticky for mobile displays */
    if ($(window).width() < 768) {
        $header.addClass("sticky");
    } else {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100)
                $header.addClass("sticky");
            else
                $header.removeClass("sticky");
        });
    }

    /* Smooth scroll links */
    $(".scrollTo").bind("click", function(e) {
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 700);
        e.preventDefault();
    });

    /* Portfolio gallery */
    baguetteBox.run('.portfolio', {
        animation: 'fadeIn',
        afterShow: function() {
            $header.addClass('animated fadeOutUp');
        },
        afterHide: function() {
            $header.toggleClass('fadeOutUp fadeInDown');
        }
    });

    /* Clients carousel */
    $clients.owlCarousel({
        items: 5,
        loop: true,
        center: true,
        dotsSpeed: 400,
        autoplay: true
    });

    /* Google map */
    map = new GMaps({
        el: '#map',
        center: new google.maps.LatLng(lat, lng),
        zoom: 15,
        scrollwheel: false,
        zoomControl: false,
        panControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        overviewMapControl: false,
        styles: [{
            "featureType": "water",
            "elementType": "all",
            "stylers": [{
                "hue": "#e9ebed"
            }, {
                "saturation": -78
            }, {
                "lightness": 67
            }, {
                "visibility": "simplified"
            }]
        }, {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{
                "hue": "#ffffff"
            }, {
                "saturation": -100
            }, {
                "lightness": 100
            }, {
                "visibility": "simplified"
            }]
        }, {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [{
                "hue": "#bbc0c4"
            }, {
                "saturation": -93
            }, {
                "lightness": 31
            }, {
                "visibility": "simplified"
            }]
        }, {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [{
                "hue": "#ffffff"
            }, {
                "saturation": -100
            }, {
                "lightness": 100
            }, {
                "visibility": "off"
            }]
        }, {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [{
                "hue": "#e9ebed"
            }, {
                "saturation": -90
            }, {
                "lightness": -8
            }, {
                "visibility": "simplified"
            }]
        }, {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [{
                "hue": "#e9ebed"
            }, {
                "saturation": 10
            }, {
                "lightness": 69
            }, {
                "visibility": "on"
            }]
        }, {
            "featureType": "administrative.locality",
            "elementType": "all",
            "stylers": [{
                "hue": "#2c2e33"
            }, {
                "saturation": 7
            }, {
                "lightness": 19
            }, {
                "visibility": "on"
            }]
        }, {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [{
                "hue": "#bbc0c4"
            }, {
                "saturation": -93
            }, {
                "lightness": 31
            }, {
                "visibility": "on"
            }]
        }, {
            "featureType": "road.arterial",
            "elementType": "labels",
            "stylers": [{
                "hue": "#bbc0c4"
            }, {
                "saturation": -93
            }, {
                "lightness": -2
            }, {
                "visibility": "simplified"
            }]
        }]
    });

    /* Gmap overlay */
    map.drawOverlay({
        lat: map.getCenter().lat(),
        lng: map.getCenter().lng(),
        content: '<div class="map-overlay"><i class="icon-map-marker"></i><h3 class="map-label">Adagio</h3></div>',
        verticalAlign: 'top',
        horizontalAlign: 'center',
        layer: 'overlayMouseTarget'
    });
});
