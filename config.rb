require 'breakpoint'
require 'bootstrap-sass'

environment = :dev

# Compass Config
preferred_syntax = :sass
http_path = '/'
project_path = "src"
css_dir = 'css'
sass_dir = 'stylesheet'
javascripts_dir = 'javascript'
images_dir = 'images'
http_generated_images_path = 'images'
fonts_dir = 'fonts'
relative_assets = false

sourcemap = (environment == :production) ? false : true
output_style = (environment == :production) ? :compressed : :expanded
line_comments = (environment == :dev) ? true : false
sass_options = (environment == :dev && debug == true) ? {:debug_info => true} : {}

Sass::Script::Number.precision = 5

