var gulp   = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');

gulp.task('javascript', function() {
  return  gulp.src('./src/javascript/**/*.js')
    .pipe(concat('app.js'))
    .pipe(uglify().on('error', function(e) { console.log('\x07',e.message); return this.end(); }))
    .pipe(gulp.dest('./build'));
});