var es     = require('event-stream'),
    concat = require('gulp-concat'),
    gulp   = require('gulp'),
    sass   = require('gulp-ruby-sass'),
    errors = require('../util/handleErrors'),
    cmq    = require('gulp-combine-media-queries'),
    prefix = require('gulp-autoprefixer'),
    minify = require('gulp-minify-css');

gulp.task('stylesheet', ['images'], function () {
  var vendor = gulp.src('./src/stylesheet/vendor/*.css');
  var app = gulp.src('./src/stylesheet/*.{sass, scss}')
    .pipe(sass({
      compass: true,
      style: 'compressed'
    }))
    .on('error', errors);

  return es.concat(vendor, app)
    .pipe(concat('app.css'))
    .pipe(cmq({
      log: true
    }))
    .pipe(prefix('> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1'))
    .pipe(minify())
    .pipe(gulp.dest('./build'));
});
