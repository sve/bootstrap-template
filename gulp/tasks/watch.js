var gulp = require('gulp');

gulp.task('watch', ['setWatch', 'browserSync'], function() {
  gulp.watch('src/javascript/**', ['javascript']);
  gulp.watch('src/stylesheet/**', ['stylesheet']);
  gulp.watch('src/images/**'    , ['images']);
  gulp.watch('src/fonts/**'     , ['fonts']);
  gulp.watch('src/index.html'   , ['markup']);
});
